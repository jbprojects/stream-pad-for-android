package com.streampad;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Browser;
import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.*;
import android.widget.*;
import com.streampad.art_pad.ArtPad;
import com.streampad.js_interfaces.JSI_StreamPad;
import com.streampad.utils.Base64;
import com.streampad.utils.SpaceTokenizer;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class StreamPad extends Activity {

    private WebView mWebView;
    private final Activity activity = this;
    private static Bundle currConfBundle;
    private ImageButton btnGoBack;
    private ImageButton btnGoFoward;
    private ImageButton btnRefresh;
    private ImageButton btnStop;
    private ImageButton btnSettings;
    private ImageButton slideButton;
    Button btnToggleImages;
    private Button btnGotoUrl;
    private ImageButton btnHome;

    private SharedPreferences configSettings;
    private SharedPreferences.Editor configEditor;
    String currImgString;
    long currImgID;
    long oldImgID;
    long currStoryID;
    private SlidingDrawer slidingDrawer;
    public ProgressDialog progressDialog;
    private MultiAutoCompleteTextView autoCompleteTextView;
    ArrayList aListHistUrls;
    ArrayList aListHistTitles;
    private final String strHomeUrl = "file:///android_asset/index.html";
    private final String strSettingsSaved = "Settings saved.";
    static final int CAMERA_REQUEST = 3;
    static final int GALLERY_REQUEST = 4;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        requestWindowFeature(Window.FEATURE_PROGRESS);
        configSettings = this.getPreferences(MODE_WORLD_WRITEABLE);
        System.out.println("ElasticPad onCreate: ");
        CookieSyncManager.createInstance(activity);

        configEditor = configSettings.edit();
        currConfBundle = getConfBundle();
        int CURR_SCREEN_ORIENT = currConfBundle.getInt("confScreenOrient", 1);
        // this.setRequestedOrientation(CURR_SCREEN_ORIENT);
        setContentView(R.layout.sp_browser);
        currImgID = System.currentTimeMillis() / 1000;
        oldImgID = currImgID;
        currStoryID = currImgID;
        currImgString = "noQvalue";
        btnGoBack = (ImageButton) findViewById(R.id.btn_go_back);
        btnGoFoward = (ImageButton) findViewById(R.id.btn_go_foward);
        btnRefresh = (ImageButton) findViewById(R.id.btn_refresh);
        btnStop = (ImageButton) findViewById(R.id.btn_stop);
        btnHome = (ImageButton) findViewById(R.id.btn_Home);
        btnSettings = (ImageButton) findViewById(R.id.btn_settings);
        btnGotoUrl = (Button) findViewById(R.id.btn_gotourl);
        String[] fullBlist;

        btnGoBack = (ImageButton) findViewById(R.id.btn_go_back);
        btnGoFoward = (ImageButton) findViewById(R.id.btn_go_foward);
        btnRefresh = (ImageButton) findViewById(R.id.btn_refresh);
        btnStop = (ImageButton) findViewById(R.id.btn_stop);
        btnHome = (ImageButton) findViewById(R.id.btn_Home);
        btnSettings = (ImageButton) findViewById(R.id.btn_settings);
        btnGotoUrl = (Button) findViewById(R.id.btn_gotourl);
        autoCompleteTextView = (MultiAutoCompleteTextView) findViewById(R.id.aCompTextView);
        autoCompleteTextView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        autoCompleteTextView.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
        autoCompleteTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {


            @Override
            public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
                // TODO Auto-generated method stub
                if (arg1 == EditorInfo.IME_ACTION_DONE) {
                    try {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(autoCompleteTextView.getWindowToken(), 0);
                        mWebView.loadUrl(autoCompleteTextView.getText().toString());
                        slidingDrawer.close();
                    } catch (Exception e) {
                        showDaToast(e.toString());
                    }
                    // search pressed and perform your functionality.
                }
                return false;
            }

        });
        // you can also prompt the user with a hint
        autoCompleteTextView.setHint("http://");
        autoCompleteTextView.setTokenizer(new SpaceTokenizer());
        slideButton = (ImageButton) findViewById(R.id.slideButton);
        slidingDrawer = (SlidingDrawer) findViewById(R.id.slidingDrawer);
        slidingDrawer.setOnDrawerOpenListener(new SlidingDrawer.OnDrawerOpenListener() {
            @Override
            public void onDrawerOpened() {
                System.out.println("slide drawer opened");
                setHistoryLinks();

            }
        });

        slidingDrawer.setOnDrawerCloseListener(new SlidingDrawer.OnDrawerCloseListener() {
            @Override
            public void onDrawerClosed() {
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(autoCompleteTextView.getWindowToken(), 0);
                } catch (Exception e) {
                    System.out.println("setOnDrawerCloseListener: " + e.toString());
                }
                System.out.println("slide drawer closed");
            }
        });


        btnGoBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (mWebView.canGoBack()) {
                    if (mWebView.getUrl().equals("about:blank")) {
                        mWebView.loadUrl(strHomeUrl);
                    } else {
                        mWebView.goBack();

                    }


                }

            }
        });


        btnGoFoward.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (mWebView.canGoForward()) {
                    if (mWebView.getUrl().equals("about:blank")) {
                        mWebView.loadUrl(strHomeUrl);
                    } else {
                        mWebView.goForward();

                    }

                }

            }
        });


        btnRefresh.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (mWebView.getUrl().equals("about:blank")) {
                    mWebView.loadUrl(strHomeUrl);
                } else {
                    mWebView.reload();
                }


            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                mWebView.stopLoading();
            }
        });

        btnSettings.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                doLoadLclUrl("settings.html");
            }
        });

        btnHome.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    mWebView.loadUrl(strHomeUrl);
                } catch (Exception e) {
                    showDaToast(e.toString());
                }


            }
        });
        btnGotoUrl.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                try {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(autoCompleteTextView.getWindowToken(), 0);
                    String strUrlString = autoCompleteTextView.getText().toString();
                    if (strUrlString.toLowerCase().startsWith("http")) {

                    } else {
                        strUrlString = "http://" + strUrlString;
                    }
                    mWebView.loadUrl(strUrlString);
                    slidingDrawer.close();
                } catch (Exception e) {
                    showDaToast(e.toString());
                }


            }
        });


        // webview browser
        mWebView = (WebView) findViewById(R.id.webview);
        mWebView.clearSslPreferences();
        mWebView.enablePlatformNotifications();
        mWebView.setWebViewClient(new HelloWebViewClient());
        mWebView.addJavascriptInterface(new JSI_StreamPad(this), "app");
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        try {
            Method m = WebSettings.class.getMethod("setDomStorageEnabled", new Class[]{boolean.class});
            m.invoke(mWebView.getSettings(), true);
        } catch (Exception e) {
            showDaToast(e.toString());
        }
        boolean CURR_SHOW_WEB_IMAGES = Boolean.parseBoolean(currConfBundle.getString("confShowWebImgs"));
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setLoadsImagesAutomatically(CURR_SHOW_WEB_IMAGES);
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.setWebChromeClient(new ChromeClient());
        mWebView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.0; en-us; Droid Build/ESD20) AppleWebKit/530.17 (KHTML, like Gecko) Version/4.0 Mobile Safari/530.17");
        registerForContextMenu(mWebView);
        mWebView.loadUrl(strHomeUrl);


    }


    private class ChromeClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView mWebView, int newProgress) {
            activity.setProgress(newProgress * 100);

        }

        @Override
        public void onConsoleMessage(String message, int lineNumber, String sourceID) {
            System.out.println("onConsoleMessage: " + message + " -- From line "
                    + String.valueOf(lineNumber) + " of "
                    + sourceID);
            // super.onConsoleMessage(message, lineNumber, sourceID);

        }


    }

    private class HelloWebViewClient extends WebViewClient {
        @Override
        public void onReceivedError(WebView mWebView, int errorCode, String description, String failingUrl) {
            Toast.makeText(getApplicationContext(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onPageStarted(WebView mWebView, String url, Bitmap favicon) {
            System.out.print("onPageStarted: " + url);
            // TODO Auto-generated method stub
            // Toast.makeText(mContext, "page started: " + url, Toast.LENGTH_LONG).show();
            CookieSyncManager.getInstance().sync();
            super.onPageStarted(mWebView, url, favicon);
        }

        @Override
        public void onReceivedHttpAuthRequest(android.webkit.WebView view, android.webkit.HttpAuthHandler handler, java.lang.String host, java.lang.String realm) {
            // TODO Auto-generated method stub
            // Toast.makeText(mContext, "page started: " + url, Toast.LENGTH_LONG).show();
            CookieSyncManager.getInstance().sync();
            System.out.print("onReceivedHttpAuthRequest: " + host);
            super.onReceivedHttpAuthRequest(view, handler, host, realm);
        }

        @Override
        public void onPageFinished(WebView mWebView, String url) {
            // TODO Auto-generated method stub
            //  Toast.makeText(mContext, "page finished: " + url, Toast.LENGTH_LONG).show();
            System.out.print("onPageFinished: " + url);
            CookieSyncManager.getInstance().sync();
            if (url.contains("elastic-pad") || url.contains("facebook")) {

            }
            if (url.contains("file://") || url.contains("javascript:")) {
                mWebView.getSettings().setBuiltInZoomControls(false);
            } else {
                autoCompleteTextView.setText(url);
                mWebView.getSettings().setBuiltInZoomControls(true);
            }
            super.onPageFinished(mWebView, url);
            // pumpToUrlString();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView mWebView, String theUrl) {
            return false;
        }

        public void onReceivedSslError(WebView mWebView, SslErrorHandler handler, SslError errer) {
            System.out.print("onReceivedSslError: " + errer);
            handler.proceed();
        }

    }




    public class CustomAdapater extends ArrayAdapter implements Filterable {
        private ArrayList<String> mData;
        private final ArrayList<String> orig;
        private final ArrayList<String> suggestions;
        final LayoutInflater inflater;

        @SuppressWarnings("unchecked")
        public CustomAdapater(Context context, ArrayList<String> al) {
            super(context, R.layout.main_list_item, al);
            inflater = LayoutInflater.from(context);
            mData = al;
            this.add(mData);
            orig = (ArrayList<String>) mData.clone();
            this.suggestions = new ArrayList<String>();
        }

        @Override
        public int getCount() {
            return suggestions.size();
        }

        @Override
        public Object getItem(int position) {
            try {
                return suggestions.get(position);
            } catch (Exception e) {
                System.out.println("CustomAdapater getItem(): " + e.toString());
                return suggestions.get(suggestions.size());
            }
        }

        @Override
        public Filter getFilter() {
            Filter myFilter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        suggestions.clear();
                        for (String s : orig) {
                            if (s != null && s.contains(constraint))
                                suggestions.add(s);
                        }
                        filterResults.values = suggestions;
                        filterResults.count = suggestions.size();
                    }
                    return filterResults;
                }

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence contraint, FilterResults results) {
                    mData = (ArrayList<String>) results.values;
                    notifyDataSetChanged();
                }
            };
            return myFilter;
        }
    }


    public void showDaToast(final String toast) {
        Toast.makeText(this, toast, Toast.LENGTH_LONG).show();
    }

    Bundle getConfBundle() {
        Bundle theConfBundle = new Bundle();
        theConfBundle.putString("confBrowBkgd", configSettings.getString("confBrowBkgd", "#800000"));
        theConfBundle.putString("confShowWebImgs", configSettings.getString("confShowWebImgs", "true"));
        theConfBundle.putInt("confScreenOrient", configSettings.getInt("confScreenOrient", 1));
        return theConfBundle;
    }

    ArrayList<String> getHistArrayList() {

        ArrayList<String> fullLstring = new ArrayList<String>();
        System.out.println("getCurrHistory new string:");

        int mCurCount = 0;
        String strLastUrl = "";
        String regex = "^(ww[a-zA-Z0-9-]{0,}\\.)";

        Cursor mCur = activity.managedQuery(Browser.BOOKMARKS_URI, Browser.HISTORY_PROJECTION, null, null, "URL");
        mCur.moveToFirst();
        if (mCur.moveToFirst() && mCur.getCount() > 0) {

            while (!mCur.isAfterLast()) {
                String strTmpTtl = mCur.getString(Browser.HISTORY_PROJECTION_TITLE_INDEX);
                String strTmpUrl = mCur.getString(Browser.HISTORY_PROJECTION_URL_INDEX);

                try {

                    if (strTmpUrl.contains("?")) {
                        strTmpUrl = strTmpUrl.substring(0, strTmpUrl.indexOf("?"));
                    }


                    if ((strTmpTtl.length() < 1) || (strTmpTtl.toLowerCase().startsWith("http"))) {
                        URL myUrl = new URL(strTmpUrl);


                        strTmpTtl = myUrl.getHost();
                        System.out.println("strTmpTtl clean: " + strTmpTtl);
                        // strTmpTtl = strTmpUrl;
                    }

                    // System.out.println("strTmpTtl[" + mCurCount + "]: " + strTmpTtl);
                    // System.out.println("strTmpUrl[" + mCurCount + "]: " + strTmpUrl);
                    if (strTmpUrl.equals(strLastUrl)) {
                    // System.out.println("strTmpUrl Duplicat: " + strTmpUrl);
                    } else {
                        System.out.println("strTmpUrl clean: " + strTmpUrl);
                        strTmpUrl = strTmpUrl.replace(",", "");
                        fullLstring.add(strTmpUrl);


                    }

                } catch (Exception e) {
                    System.out.println("mcur: " + e.toString());
                }

                strLastUrl = strTmpUrl;
                mCurCount++;
                strTmpTtl = null;
                strTmpUrl = null;
                mCur.moveToNext();
            }


        }
        // System.out.println("fullLstring: " + fullLstring);
        return fullLstring;
    }


    void setHistoryLinks() {
        autoCompleteTextView.setAdapter(new CustomAdapater(this, getHistArrayList()));
    }


    public static String getConfValString(String theKey) {
        String strTheKey = "noQvalue";
        try {
            strTheKey = currConfBundle.getString(theKey);
        } catch (Exception err) {
            System.out.println("Error.getConValString: " + err);
        }
        return strTheKey;
    }

    public Integer getConfValInt(String theKey) {

        int strTheKey = 1234;
        try {
            strTheKey = currConfBundle.getInt(theKey);
        } catch (Exception err) {
            System.out.println("Error.getConValString: " + err);
        }
        return strTheKey;
    }


    public void putConfValString(String theKey, String theVal) {


        configEditor = configSettings.edit();
        configEditor.putString(theKey, theVal);
        configEditor.commit();
        currConfBundle = getConfBundle();
        showDaToast(strSettingsSaved);


    }

    public void putConfValInt(String theKey, Integer theVal) {

        configEditor = configSettings.edit();
        configEditor.putInt(theKey, theVal);
        configEditor.commit();
        currConfBundle = getConfBundle();

        showDaToast(strSettingsSaved);

    }

    void doLoadLclUrl(String theLclUstr) {
        String fullUrl = "file:///android_asset/" + theLclUstr;
        mWebView.loadUrl(fullUrl);
    }

    public void doStreamPic(String fromType) {
        if (fromType.equals("camera")) {
            //   //  // startEpCamera();
            Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(pictureIntent, CAMERA_REQUEST);
            // pictureIntent.setRequestedOrientation(1);
        } else {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK, (MediaStore.Images.Media.EXTERNAL_CONTENT_URI));
            startActivityForResult(pickPhoto, GALLERY_REQUEST);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {

        switch (requestCode) {

            case (CAMERA_REQUEST):

                if (resultCode == Activity.RESULT_OK) {

                    String[] projection = {
                            MediaStore.Images.Thumbnails._ID,  // The columns we want
                            MediaStore.Images.Thumbnails.IMAGE_ID,
                            MediaStore.Images.Thumbnails.KIND,
                            MediaStore.Images.Thumbnails.DATA};
                    String selection = MediaStore.Images.Thumbnails.KIND + "=" + // Select only mini's
                            MediaStore.Images.Thumbnails.MINI_KIND;

                    String sort = MediaStore.Images.Thumbnails._ID + " DESC";

                    //At the moment, this is a bit of a hack, as I'm returning ALL images, and just taking the latest one. There is a better way to narrow this down I think with a WHERE clause which is currently the selection variable
                    Cursor myCursor = this.managedQuery(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, projection, selection, null, sort);

                    long imageId = 0l;
                    long thumbnailImageId = 0l;
                    String thumbnailPath = "";

                    try {
                        myCursor.moveToFirst();
                        imageId = myCursor.getLong(myCursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails.IMAGE_ID));
                        thumbnailImageId = myCursor.getLong(myCursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID));
                        thumbnailPath = myCursor.getString(myCursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails.DATA));
                    } finally {
                        myCursor.close();
                    }

                    //Create new Cursor to obtain the file Path for the large image

                    String[] largeFileProjection = {
                            MediaStore.Images.ImageColumns._ID,
                            MediaStore.Images.ImageColumns.DATA
                    };

                    String largeFileSort = MediaStore.Images.ImageColumns._ID + " DESC";
                    myCursor = this.managedQuery(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, largeFileProjection, null, null, largeFileSort);
                    String largeImagePath = "";

                    try {
                        myCursor.moveToFirst();

                        //This will actually give yo uthe file path location of the image.
                        largeImagePath = myCursor.getString(myCursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.DATA));
                    } finally {
                        myCursor.close();
                    }
                    Uri artpadPhotoUri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(imageId));
                    try {
                        InputStream is = getApplicationContext().getContentResolver().openInputStream(artpadPhotoUri);
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 7;
                        Bitmap bitmap = BitmapFactory.decodeStream(is, null, null);
                       //  artpadBitmap = bitmap;
                        doArtPadBitmap(scaleBitmap(bitmap));

                        // encBitmap(scaleBitmap(bitmap));
                    } catch (Exception e) {
                        System.out.println("CAMERA_REQUEST: " + e.toString());
                    }
                } else {
                    //CAMERA MODE CANCELLED
                }
                break;
            case (GALLERY_REQUEST):
                if (resultCode == Activity.RESULT_OK) {
                    Uri artpadPhotoUri = data.getData();
                    try {
                        Bitmap bitmap = scaleBitmap(MediaStore.Images.Media.getBitmap(this.getContentResolver(), artpadPhotoUri));
                        doArtPadBitmap(scaleBitmap(bitmap));
                       //  encBitmap(bitmap);
                    } catch (Exception e) {
                        System.out.println("GALLERY_REQUEST: " + e.toString());
                    }
                } else {
                    //gallery MODE CANCELLED
                }
                break;
            case (11):
                if (resultCode == Activity.RESULT_OK) {

                    try {
                        String source = data.getStringExtra("encdBmp");
                        System.out.println("ArtPadRequest.source: " + source);
                        oldImgID = currImgID;
                        currImgID = System.currentTimeMillis() / 1000;
                        currImgString = source;
                        mWebView.loadUrl("javascript:showStreamPic('" + source + "');");
                    } catch (Exception e) {
                        System.out.println("ArtPadRequest: " + e.toString());
                    }
                } else {
                    //gallery MODE CANCELLED
                }
                break;

            default: {
                System.out.println("Its default");
            }

        }



    }

    public Bitmap scaleBitmap(Bitmap theBitmap) {
        // Get the ImageView and its bitmap
        System.out.println("scaleView called::");


        // Get current dimensions AND the desired bounding box
        int width = theBitmap.getWidth();
        int height = theBitmap.getHeight();
        int bounding = 400;
        Log.i("Test", "original width = " + Integer.toString(width));
        Log.i("Test", "original height = " + Integer.toString(height));
        Log.i("Test", "bounding = " + Integer.toString(bounding));

        // Determine how much to scale: the dimension requiring less scaling is
        // closer to the its side. This way the image always stays inside your
        // bounding box AND either x/y axis touches it.
        float xScale = ((float) bounding) / width;
        float yScale = ((float) bounding) / height;
        float scale = (xScale <= yScale) ? xScale : yScale;
        Log.i("Test", "xScale = " + Float.toString(xScale));
        Log.i("Test", "yScale = " + Float.toString(yScale));
        Log.i("Test", "scale = " + Float.toString(scale));

        // Create a matrix for the scaling and add the scaling data
        Matrix matrix = new Matrix();
        matrix.postScale(scale, scale);

        // Create a new bitmap and convert it to a format understood by the ImageView
        Bitmap scaledBitmap = Bitmap.createBitmap(theBitmap, 0, 0, width, height, matrix, true);
        width = scaledBitmap.getWidth(); // re-use
        height = scaledBitmap.getHeight(); // re-use
        // BitmapDrawable result = new BitmapDrawable(scaledBitmap);
        Log.i("Test", "scaled width = " + Integer.toString(width));
        Log.i("Test", "scaled height = " + Integer.toString(height));
        return scaledBitmap;
    }

    // saving the file to Gallery
    public void saveBitmap(Bitmap bitmap) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaStorageDir = Environment.getExternalStorageDirectory();
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                System.out.println("saveFile: failed to create directory");
                return;
            }
        }
        try {
            String saved = MediaStore.Images.Media.insertImage(this.getContentResolver(), bitmap, "title", "description");
            Uri sdCardUri = Uri.parse("file://" + Environment.getExternalStorageDirectory());
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, sdCardUri));
            finish();
            System.out.println("StreamPad saveFile: ");
        } catch (Exception e) {
            System.out.println("saveFile: " + e.toString());
            e.printStackTrace();

        }
    }


    public void encBitmap(Bitmap theBitmap) {
        try {
           //  artpadBitmap = theBitmap;
            oldImgID = currImgID;
            currImgID = System.currentTimeMillis() / 1000;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            theBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            baos.close();
            byte[] bMapArray = baos.toByteArray();
            String encodedImage = Base64.encodeBytes(bMapArray);
            System.out.println("encBitmap: " + encodedImage);

            mWebView.loadUrl("javascript:showStreamPic('" + encodedImage + "');");
            //  String isdone = imgUploader.doImgUpload(bMapArray, "New Pic");
        } catch (Exception e) {
            System.out.println("encBitmap.error: " + e.toString());
        }
    }

    public void decBitmap(String theEncStr) {
        try {
            byte[] decodedString = Base64.decode(theEncStr);
        } catch (Exception e) {
            System.out.println("decBitmap: " + e.toString());
        }
    }

    public void doUrlSlider() {
        try {
            this.runOnUiThread(new Runnable() {
                public void run() {
                    slidingDrawer.open();
                }
            });


        } catch (Exception e) {
            System.out.println("doUrlSlider" + e.toString());
        }

    }

    public void doPageLoad() {
        String abgclr = "#800000";
        try {
            abgclr = currConfBundle.getString("confBrowBkgd");
            mWebView.loadUrl("javascript:doPageLoad('" + abgclr + "','noQvalue','noQvalue');");
        } catch (Exception e) {
            System.out.println("doPageLoad" + e.toString());
        }
    }


    public void doStoryShare(String strNwork, String strSTitle, String strSDesc) {
        try {
            this.runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog = ProgressDialog.show(StreamPad.this, "", "Please wait...", true, true);
                }
            });


        } catch (Exception e) {
            System.out.println("doStoryShare dialog" + e.toString());
        }

        try {

            if (oldImgID == currImgID) {
                doStoryUpload(null, strNwork, strSTitle, strSDesc);
            } else {

                oldImgID = currImgID;
                currImgID = System.currentTimeMillis() / 1000;
                byte[] decodedString = Base64.decode(currImgString);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length,null);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                baos.close();
                byte[] bMapArray = baos.toByteArray();
                doStoryUpload(bMapArray, strNwork, strSTitle, strSDesc);
                // System.out.println("image_str: " + image_str);
            }

        } catch (Exception e) {
            progressDialog.dismiss();
            System.out.println("doStoryShare: " + e.toString());
        }

    }

    public String doStoryUpload(byte[] byte_arr, String strSnwork, String strStoryTitle, String strStoryDesc) {
        String strCurrSID = Long.toString(currStoryID);
        String strOutput = "noQvalue";
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("do", "add"));
        nameValuePairs.add(new BasicNameValuePair("sttl", strStoryTitle));
        nameValuePairs.add(new BasicNameValuePair("sdesc", strStoryDesc));
        nameValuePairs.add(new BasicNameValuePair("sdesc", strStoryDesc));
        nameValuePairs.add(new BasicNameValuePair("stime", strCurrSID));
        nameValuePairs.add(new BasicNameValuePair("snwork", strSnwork));
        if (byte_arr != null) {
            String strImgID = Long.toString(currImgID);
            String image_str = Base64.encodeBytes(byte_arr);
            nameValuePairs.add(new BasicNameValuePair("simg", image_str));
            // System.out.println("image_str: " + image_str);
        }


        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost("http://www.elastic-pad.com/story/index.php");
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = httpclient.execute(httppost);
            strOutput = convertResponseToString(response);
            if (strOutput.startsWith("http")) {
                mWebView.loadUrl(strOutput);
            }
            System.out.println("the_story_response: " + strOutput);
        } catch (Exception e) {
            showDaToast("Oooops. Could not connect. Check your connection.");
            System.out.println("Error in http connection " + e.toString());
            progressDialog.dismiss();
            return strOutput;
        }
        progressDialog.dismiss();
        return strOutput;

    }

    public String convertResponseToString(HttpResponse response) throws IllegalStateException, IOException {
        String res = "noQvalue";
        StringBuffer buffer = new StringBuffer();
        InputStream inputStream = response.getEntity().getContent();
        int contentLength = (int) response.getEntity().getContentLength(); //getting content length…..
        if (contentLength < 0) {
        } else {
            byte[] data = new byte[512];
            int len = 0;
            try {
                while (-1 != (len = inputStream.read(data))) {
                    buffer.append(new String(data, 0, len)); //converting to string and appending  to stringbuffer…..
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                inputStream.close(); // closing the stream…..
            } catch (IOException e) {
                e.printStackTrace();
            }
            res = buffer.toString();     // converting stringbuffer to string…..
        }
        return res;
    }

    public void doNewStoryID() {
        currImgID = System.currentTimeMillis() / 1000;
        oldImgID = currImgID;
        currStoryID = currImgID;
        mWebView.reload();
    }

    public Bitmap doBitmapFromAsset(String strBfile) {
        AssetManager assetManager = StreamPad.this.getAssets();
        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(strBfile);
            bitmap = BitmapFactory.decodeStream(istr);
        } catch (IOException e) {
            return null;
        }
        return bitmap;
    }

    public void doAssetBmp(String strBfile) {
        encBitmap(scaleBitmap(doBitmapFromAsset(strBfile)));
    }



    public static String getLocalUrl(String strLUrl) {
    return "file:///android_asset/" + strLUrl ;
    }
    /* ArtPad functions */

    public void doArtPadBmpaNull() {

    }

    public void doArtPadBitmap() {
        try {
            Intent toMain = new Intent(this, ArtPad.class);
            toMain.putExtra("encdBmp", currImgString);
            startActivityForResult(toMain, 11);
        } catch(Exception e) {
            System.out.println("doArtPadBitmap" + e.toString());
        }
    }

    public void doArtPadBitmap(final Bitmap theDoAPBitmap) {
        try {
            Intent toMain = new Intent(this, ArtPad.class);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            theDoAPBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            baos.close();
            byte[] bMapArray = baos.toByteArray();
            String encodedImage = Base64.encodeBytes(bMapArray);
            toMain.putExtra("encdBmp", encodedImage);
            startActivityForResult(toMain, 11);
        } catch(Exception e) {
            System.out.println("doArtPadBitmap" + e.toString());
        }
    }

    public void doImgLoad() {
        try {
            if(currImgString.equalsIgnoreCase("noQvalue")) {

            }   else {
            mWebView.loadUrl("javascript:showStreamPic('" + currImgString + "');");
            }
        } catch(Exception e) {
            System.out.println("doImgLoad" + e.toString());
        }
    }
    public void doImgEdit() {
        try {
            if(currImgString.equalsIgnoreCase("noQvalue")) {

            }   else {
                doArtPadBitmap();
               //  mWebView.loadUrl("javascript:showStreamPic('" + currImgString + "');");
            }
        } catch(Exception e) {
            System.out.println("doImgLoad" + e.toString());
        }
    }

}
