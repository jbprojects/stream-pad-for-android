package com.streampad.utils;
 
import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.*;
import android.widget.Toast;

/**
 * Created by IntelliJ IDEA.
 * User: boss
 * Date: 20-10-2012
 * Time: 3:24
 * To change this template use File | Settings | File Templates.
 */
public class StreamPadWebView extends WebView {

    Context mContext;
    public StreamPadWebView(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        mContext = context;
        setWebViewClient();
    }

    public StreamPadWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

        setWebViewClient();
    }

    @Override
    public boolean onCheckIsTextEditor() {
        return true;
    }

    boolean setWebViewClient()
    {

        setScrollBarStyle(SCROLLBARS_INSIDE_OVERLAY);
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus(View.FOCUS_DOWN);

        WebSettings webSettings = getSettings();
        webSettings.setSavePassword(false);
        webSettings.setSaveFormData(false);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setSupportZoom(false);
        webSettings.setUseWideViewPort(true);
        //addJavascriptInterface(new DemoJavaScriptInterface(), "demo");
        this.setFocusable(true);
        this.setFocusableInTouchMode(true);
        this.requestFocus(View.FOCUS_DOWN);
        this.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_UP:
                        if (!v.hasFocus()) {
                            v.requestFocus();
                        }
                        break;
                }
                return false;
            }
        });

        setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_UP:
                        if (!v.hasFocus()) {
                            v.requestFocus();
                        }
                        break;
                }
                return false;
            }
        });


        this.setWebViewClient(new WebViewClient()
        {

            @Override
            public void onReceivedError(WebView apPopWebView, int errorCode, String description, String failingUrl) {
                Toast.makeText(getContext(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView apPopWebView, String url, Bitmap favicon) {
                System.out.print("onPageStarted: " + url);
                super.onPageStarted(apPopWebView, url, favicon);
            }

            @Override
            public void onPageFinished(WebView apPopWebView, String url) {
                System.out.print("onPageFinished: " + url);
                super.onPageFinished(apPopWebView, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView apPopWebView, String theUrl) {
                System.out.print("shouldOverrideUrlLoading: " + theUrl);
                return false;
            }
        });

        this.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView mWebView, int newProgress) {
                //   this.setProgress(newProgress * 100);

            }
            @Override
            public void onConsoleMessage(String message, int lineNumber, String sourceID) {
                System.out.println("onConsoleMessage: " + message + " -- From line "
                        + String.valueOf(lineNumber) + " of "
                        + sourceID);
                // super.onConsoleMessage(message, lineNumber, sourceID);

            }

            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                System.out.println("+-------------------------------");
                System.out.println("|WebChromeClient onJsAlert	" + message);
                System.out.println("+-------------------------------");
                result.confirm();
                return true;
            }
        });



        // loadUrl("http://www.google.com");

        return true;
    }


}