package com.streampad.js_interfaces;

import com.streampad.StreamPad;

/**
 * Created by IntelliJ IDEA.
 * User: boss
 * Date: 16-07-2013
 * Time: 10:24
 * To change this template use File | Settings | File Templates.
 */
public class JSI_StreamPad extends StreamPad {
    final StreamPad streamPad;
    public JSI_StreamPad(StreamPad theStreamPad) {
        streamPad = theStreamPad;

    }

    /*
   * functions for setting and getting config values
    */
    public void setConfValString(String theString, String theVal) {
        streamPad.putConfValString(theString, theVal);
    }
    public void setConfValInt(String theString, String theVal) {
        streamPad.putConfValInt(theString, Integer.parseInt(theVal));
    }

    public String fetchConfValString(String theString) {
        String confStr = streamPad.getConfValString(theString);
        return confStr;
    }
    public String fetchConfValInt(String theString) {
        String confStr = String.valueOf(streamPad.getConfValInt(theString));
        return confStr;
    }



    public void setStreamPic(String fromType) {
        streamPad.doStreamPic(fromType);
    }

    public void getUrlSlider() {
        streamPad.doUrlSlider();
    }

    /* Sends configuration for the loaded page */
    public void setPageLoad() {
        streamPad.doPageLoad();
    }
    
    public void setStoryShare(String sNwork, String  stitle, String  sdesc) {
        System.out.println("setStoryShare" + sNwork + ";;" + stitle + "::" + sdesc);
        streamPad.doStoryShare(sNwork, stitle, sdesc);
    }
    public void setNewStoryID() {
        streamPad.doNewStoryID();
    }
    public void setAssetBmp(String strBname) {
        streamPad.doAssetBmp(strBname);
    }
    public void setImgLoad() {
        streamPad.doImgLoad();
    }
    public void setImgEdit() {
        streamPad.doImgEdit();
    }
}
