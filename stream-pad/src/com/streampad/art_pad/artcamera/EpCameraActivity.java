package com.streampad.art_pad.artcamera;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import com.streampad.art_pad.ArtPad;
import com.streampad.StreamPad;
import com.streampad.R;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

/**
 * Created by IntelliJ IDEA.
 * User: boss
 * Date: 14-10-2012
 * Time: 1:50
 * To change this template use File | Settings | File Templates.
 */
public class EpCameraActivity  extends StreamPad {

    private Camera mCamera;
    private EpCameraPreview mPreview;
    ArtPad artPad;




    public void drawGui() {
        setContentView(R.layout.camera_activity);
        mCamera = getCameraInstance();
        mPreview = new EpCameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);

        Button captureButton = (Button) findViewById(R.id.button_capture);
        captureButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get an image from the camera
                        try {

                            doTakePicture();
                        }       catch(Exception e) {
                            System.out.println("mCamera.takePicture: " + e.toString());
                        }
                    }
                }
        );
    }
    public void doTakePicture() {
        try {
        mCamera.stopPreview();
        mCamera.takePicture(null, null, mPicture, mPicture);
        } catch(Exception e){
            System.out.println("doTakePicture: " + e.toString());
        }
    }
     
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        drawGui();
        // Create an instance of Camera
      //

        // Create our Preview view and set it as the content of our activity.

    }
    

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            System.out.println("Camera error:" + e.toString());
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }


    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            System.out.println("artPad saveFile");
            // Drawable drw = img.getDrawable();
            // Bitmap savedPic = ((BitmapDrawable)drw).getBitmap();
            OutputStream fOut = null;
            /*
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File mediaStorageDir = Environment.getExternalStorageDirectory();
            //File mediaStorageDir = new File(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "Pimped pictures");
            if (! mediaStorageDir.exists()){
                if (! mediaStorageDir.mkdirs()){
                    Log.d("ArtPad", "failed to create directory");
                    return;
                }
            }
            */
            try {

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 1;
                Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length, options);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                picture.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                baos.close();
                /*



                 File picFile = new File(mediaStorageDir.getPath() + File.separatorChar + "artPad_"+ timeStamp + ".jpg");
                 fOut = new FileOutputStream(picFile);

                 fOut.write(data);

                // picture.compress(Bitmap.CompressFormat.JPEG, 95, fOut);
                 fOut.flush();
                 fOut.close();
                 System.out.println("artPad saveFile: " + mediaStorageDir.getPath() + File.separatorChar + "artPad_"+ timeStamp + ".jpg");
                Uri picUri = Uri.fromFile(picFile);
                // sendToWView(picFile.toString());
                */
               // showDaToast("Trying from EpCamera");
               // mCamera.release();
              //  mCamera = null;
                doArtPadBitmap(picture);
               // mCamera.release();
               // mCamera = null;
                finish();
                /*
                Intent i = new Intent(getApplicationContext(), ArtPad.class);
                i.putExtra("byteArray", baos.toByteArray());
                startActivity(i);
                 */
               // setApImgBitmap(picture);
              // finish();
            } catch (Exception e) {

                System.out.println("onPictureTaken: " + e.toString());
            }
        }
    };

}