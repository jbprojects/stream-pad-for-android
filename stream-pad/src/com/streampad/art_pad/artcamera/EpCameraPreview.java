package com.streampad.art_pad.artcamera;

/**
 * Created by IntelliJ IDEA.
 * User: boss
 * Date: 14-10-2012
 * Time: 1:56
 * To change this template use File | Settings | File Templates.
 */

import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/** A basic Camera preview class */
public class EpCameraPreview extends SurfaceView implements SurfaceHolder.Callback {
    private SurfaceHolder mHolder;
    private Camera mCamera;

    public EpCameraPreview(Context context, Camera camera) {
        super(context);
        mCamera = camera;

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();

        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, now tell the camera where to draw the preview.
        try {
          mCamera = Camera.open();




            mCamera.setPreviewDisplay(holder);
            /*
            Camera.Parameters p = mCamera.getParameters();

            p.set("jpeg-quality", 100);
            p.set("orientation", "portrait");
            // p.set("rotation", 180);
            p.setPictureFormat(PixelFormat.JPEG);
             p.setPreviewSize(300, 400);// here w h are reversed

            mCamera.setParameters(p);
            */
            mCamera.startPreview();
        } catch (Exception e) {
            System.out.println("Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // empty. Take care of releasing the Camera preview in your activity.
        if (mCamera != null) {

        mCamera.stopPreview();
        mCamera.setPreviewCallback(null);
        mCamera.release();
        mCamera = null;
        }

    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null){
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e){
            System.out.println("Error starting camera preview: " + e.getMessage());
        }
    }


}
