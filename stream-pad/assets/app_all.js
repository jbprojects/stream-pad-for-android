        function setConfValInt(theKey, theVal)  {
        app.setConfValInt(theKey, theVal);
        }

        function setConfValString(theKey, theVal)  {
        if(theKey == "confBrowBkgd") {
        document.body.style.backgroundColor = theVal;
        }
        app.setConfValString(theKey, theVal);
        }

        function getConfValInt(theKey)  {
        strConf = app.fetchConfValInt(theKey);
        return strConf;
        }

        function getConfValString(theKey)  {
        strConf = app.fetchConfValString(theKey);
        return strConf;
        }

        function setStreamPic(thePicType) {
        app.setStreamPic(thePicType);
        }

        function showStreamPic(strImgstr) {
        imgStreamPic.src = "data:image/png;base64, " + strImgstr;
        }

        function setUrlSlider() {
        app.getUrlSlider();
        }

        function random_imglink(){
        var myimages=new Array()
        //specify random images below. You can have as many as you wish
        myimages[1]="icon1.jpg"
        myimages[2]="icon2.jpg"
        myimages[3]="icon4.jpg"
        myimages[4]="icon5.jpg"
        myimages[5]="icon6.jpg"
        myimages[6]="icon7.jpg"
        myimages[7]="icon8.jpg"
        myimages[8]="icon9.jpg"
        var ry=Math.floor(Math.random()*myimages.length)
        if (ry==0) {
        ry=1
        }
        imgStreamPic.src = "images/" + myimages[ry];
        try {
        app.setAssetBmp("images/" + myimages[ry]);
        }  catch(e) {
        }
        // imgStreamPic.src = "images/icon8.jpg";
        }

        function setPageLoad() {
        try {
        app.setPageLoad();
        } catch(e) {
        alert("setPageLoad.error: " + e)
        }
        }

        function setImgLoad() {
        try {
        app.setImgLoad();
        } catch(e) {
        alert("setPageLoad.error: " + e)
        }
        }
        function setImgEdit() {
        try {
        app.setImgEdit();
        } catch(e) {
        alert("setImgEdit.error: " + e)
        }
        }

        function setAPPageLoad() {
        try {
        app_artpad.getAPPageLoad();
        } catch(e) {
        alert("setPageLoad.error: " + e)
        }
        }


        // main function for ArtPad.java
        function  setAPJSComm(effectID)   {
        try {
        app_artpad.getAPJSComm(effectID);
        } catch(e) {
        alert("setAPFilterEffect: " + e)
        }
        }


        function doPageLoad(pgbgColor, theValA, theValB) {
        try {
        document.body.style.backgroundColor = pgbgColor;
        } catch(e) {
        alert(e);
        }
        try {
        document.getElementById("tblSharePop").style.backgroundColor = pgbgColor;
        } catch(e) {
        // alert(e);
        }
        }

        function showHideElement(element, showHide) {
        // alert(element);
        //function to show or hide elements
        //element variable can be string or object
        if (document.getElementById(element)) {
        element = document.getElementById(element);
        }
        if(element) {
        if(showHide == "show") {
        element.style.visibility = "visible";
        try {
        element.style.display = "block";
        } catch(e) {
        }
        } else {
        element.style.visibility = "hidden";
        try {
        element.style.display = "none";
        } catch(e) {
        }
        }
        }
        }

        function setTinnerHTML(theElemId, theInnerHtml){
        document.getElementById(theElemId).innerHTML = theInnerHtml;
        }

        function setObjBGcolor(theObj, theclr) {
        theObj.style.backgroundColor = theclr;
        }

        function setStoryShare(sNwork) {
        try {
        showHideElement('tdFormControls', 'show');
        showHideElement('epSharePop', 'hide');
        stitle = document.getElementById("taStitle").value;
        sdesc  = document.getElementById("taSdesc").value
        app.setStoryShare(sNwork, stitle, sdesc);
        } catch(e) {
        alert(e);
        }
        }
        function setNewStoryID(sNwork) {
        try {
        app.setNewStoryID();
        } catch(e) {
        // alert(e);
        }
        }
        function hideSSharePop() {
        try {
        showHideElement('tdFormControls', 'show');
        showHideElement('epSharePop', 'hide');
        } catch(e) {
        alert(e);
        }
        }
        function setSSharePop() {
        try {
        showHideElement('epSharePop', 'show');
        showHideElement('tdFormControls', 'hide');
        centerPopWin('epSharePop');
        } catch(e) {
        alert(e);
        }
        }

        /**
        * Code below taken from - http://www.evolt.org/article/document_body_doctype_switching_and_more/17/30655/
        *
        * Modified 4/22/04 to work with Opera/Moz (by webmaster at subimage dot com)
        *
        * Gets the full width/height because it's different for most browsers.
        */
        function getViewportHeight() {
        if (window.innerHeight!=window.undefined) return window.innerHeight;
        if (document.compatMode=='CSS1Compat') return document.documentElement.clientHeight;
        if (document.body) return document.body.clientHeight;

        return window.undefined;
        }
        function getViewportWidth() {
        var offset = 17;
        var width = null;
        if (window.innerWidth!=window.undefined) return window.innerWidth;
        if (document.compatMode=='CSS1Compat') return document.documentElement.clientWidth;
        if (document.body) return document.body.clientWidth;
        }

        /**
        * Gets the real scroll top
        */
        function getScrollTop() {
        if (self.pageYOffset) // all except Explorer
        {
        return self.pageYOffset;
        }
        else if (document.documentElement && document.documentElement.scrollTop)
        // Explorer 6 Strict
        {
        return document.documentElement.scrollTop;
        }
        else if (document.body) // all other Explorers
        {
        return document.body.scrollTop;
        }
        }

        function getScrollLeft() {
        if (self.pageXOffset) // all except Explorer
        {
        return self.pageXOffset;
        }
        else if (document.documentElement && document.documentElement.scrollLeft)
        // Explorer 6 Strict
        {
        return document.documentElement.scrollLeft;
        }
        else if (document.body) // all other Explorers
        {
        return document.body.scrollLeft;
        }
        }

        function centerPopWin(theObj) {
        thePopUpObj = document.getElementById(theObj);
        width = thePopUpObj.offsetWidth;
        height = thePopUpObj.offsetHeight;
        var theBody = document.getElementsByTagName("BODY")[0];
        var scTop = parseInt(getScrollTop(),10);
        var scLeft = parseInt(theBody.scrollLeft,10);
        var fullHeight = getViewportHeight();
        var fullWidth = getViewportWidth();
        thePopUpObj.style.top = (scTop + ((fullHeight - height) / 2)) + "px";
        thePopUpObj.style.left =  (scLeft + ((fullWidth - width) / 2)) + "px";
        }

        function getUnixTimeStamp() {
        ts = Math.round(new Date().getTime() / 1000);
        return ts;
        }